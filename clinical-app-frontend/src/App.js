import React from 'react';
import logo from './logo.svg';
import './App.css';
import MyRoute from  './components/MyRoute';

function App() {
  return (
    <div className="App">
      <MyRoute/>
    </div>
  );
}

export default App;
