import React, {Component} from 'react';
import axios from 'axios';
import {Link} from 'react-router-dom';

class Home extends Component {
	
	constructor()
	{
		super()
		this.state={
			patients:[]
		}
	}
	
	componentDidMount() 
	{	
		axios.get(`http://localhost:9090/clinicalrestfulapi/api/v1/patients`)
		.then(response => {
			console.log(response.data);
			this.setState({patients:response.data})
		})
		.catch(error=> {
		  console.log(error);
		})
	}
	
    render() {
		const title=<h1>Patient List</h1>
		const patients = this.state.patients;
		let rows =patients.map((item, index) => {
			 	return	(<tr>
						  <td>{++index}</td>
						  <td>{item.firstName}</td>
						  <td>{item.lastName}</td>
						  <td>{item.age}</td>
						  <td>{item.age}</td>
						  <td><Link to={'/patientDetails/'+item.id}>Add Data</Link></td>
						  <td><Link to={'/analyze/'+item.id}>Analyze</Link></td>
						</tr>)
		})
        return ( 
			<div>
			{title}
			<table>
					  <thead>
						<tr>
						  <th>S/N</th>
						  <th>First Name</th>
						  <th>Last Name</th>
						  <th>Age</th>
						</tr>
					  </thead>
					  <tbody>
							{rows}
					  </tbody>
			</table>
			<br/>
			<Link to={'/addPatient'}>Register Patient</Link>
			</div>
        );
    }
}

export default Home;