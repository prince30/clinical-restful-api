import React, { Component } from 'react';
import {Route, BrowserRouter as Router, Switch} from 'react-router-dom';
import Home from  './Home';
import AddPatient from  './AddPatient';
import CollectClinicals from  './CollectClinicals';
import AnalyzeData from  './AnalyzeData';

class MyRoute extends Component {
    render() {
        return (
            <div>
				<Router>
					<Switch>
					<Route exact path="/" component={Home} />
					<Route exact path="/addPatient" component={AddPatient} />
					<Route exact path="/analyze/:patientId" component={AnalyzeData} />
					<Route exact path="/patientDetails/:patientId" component={CollectClinicals} />
					</Switch>
				</Router>
            </div>
        );
    }
}

export default MyRoute;