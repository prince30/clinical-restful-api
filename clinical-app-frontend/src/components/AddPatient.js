import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import axios from 'axios';
import {toast} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

toast.configure() 

class AddPatient extends Component {
	
	constructor()
	{
		super()
		this.state={
			firstName:"",
			lastName:"",
			age:""
		}
		
		this.handleSubmit=this.handleSubmit.bind(this);
		this.handleChange=this.handleChange.bind(this);
	}
	
	handleChange=(event)=>{		
		this.setState({[event.target.name] : event.target.value})
	}
	
	handleSubmit=(event)=>{
		event.preventDefault();
		var postData={
			firstName:this.state.firstName,
			lastName:this.state.lastName,
			age:this.state.age
		}

		axios.post(`http://localhost:9090/clinicalrestfulapi/api/v1/patients`, postData)
		.then(response => {
			 toast("Patient added successfully",{autoClose:2000, position: toast.POSITION.BOTTOM_CENTER})  
		})
		.catch(error=> {
		  console.log(error);
		})
	}
	
    render() {
        return ( 
			<div>
				<h2>Add Patient</h2>
				<form>
				  <label for="fname">First name:</label>
				  <input type="text" id="fname" name="firstName"onChange={this.handleChange}/><br/>
				  <label for="lname">Last name:</label>
				  <input type="text" id="lname" name="lastName"onChange={this.handleChange}/><br/>
				  <label for="lname">Age:</label>
				  <input type="text" id="lname" name="age" onChange={this.handleChange}/><br/><br/>
				  <button onClick={this.handleSubmit}>Add</button>
				</form> 
				<Link to={'/'}>Go Back</Link>
			</div>
        );
    }
}

export default AddPatient;