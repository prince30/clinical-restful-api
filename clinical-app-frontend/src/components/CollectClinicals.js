import React, { Component } from 'react';
import axios from 'axios';
import {Link} from 'react-router-dom';
import {toast} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

toast.configure() 

class CollectClinicals extends Component {
	
	constructor()
	{
		super()
		this.state={
			patientInfo:"",
			componentName:"",
			componentValue:""
		}
		
		this.handleSubmit=this.handleSubmit.bind(this);
		this.handleChange=this.handleChange.bind(this);
	}
	
	componentDidMount() 
	{	
		axios.get(`http://localhost:9090/clinicalrestfulapi/api/v1/patients/` +this.props.match.params.patientId)
		.then(response => {
			console.log(response.data);
			this.setState({patientInfo:response.data})
		})
		.catch(error=> {
		  console.log(error);
		})
	}
	
	handleChange=(event)=>{		
		this.setState({[event.target.name] : event.target.value})
	}
	
	handleSubmit=(event)=>{
		event.preventDefault();
		var postData={
			patientId:this.props.match.params.patientId,
			componentName:this.state.componentName,
			componentValue:this.state.componentValue
		}

		axios.post(`http://localhost:9090/clinicalrestfulapi/api/v1/clinicals`, postData)
		.then(response => {
			 toast("Clinical data added successfully",{autoClose:2000, position: toast.POSITION.BOTTOM_CENTER})  
		})
		.catch(error=> {
		  console.log(error);
		})
	}
	
    render() {
        return (
            <div>
				  <h1>Patient Details</h1>
				  First name : {this.state.patientInfo.firstName}<br/>
				  Last name : {this.state.patientInfo.lastName}<br/>
				  Age : {this.state.patientInfo.age}
				  <h2>Add clinical Data</h2>
					<form>
					  <label>Clinical Entry Type : </label>
					  <select name="componentName" onChange={this.handleChange}>
						  <option value="bp">Blood Pressure</option>
						  <option value="hw">Height Weight</option>
						  <option value="heartRate">Heart Rate</option>
					  </select><br/><br/>
					  <label>Value : </label>
					  <input type="text" id="lname" name="componentValue" onChange={this.handleChange}/><br/><br/>
					  <button onClick={this.handleSubmit}>Add</button>
					</form> 
					<Link to={'/'}>Go Back</Link>
            </div>
        );
    }
}

export default CollectClinicals;