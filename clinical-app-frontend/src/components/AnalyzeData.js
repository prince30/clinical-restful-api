import React, {Component} from 'react';
import axios from 'axios';
import {Link} from 'react-router-dom';

class AnalyzeData extends Component {
	
	constructor()
	{
		super()
		this.state={
			clinicalData:[]
		}
	}
	
	componentDidMount() 
	{	
		axios.get(`http://localhost:9090/clinicalrestfulapi/api/v1/patients/`+this.props.match.params.patientId+`/analyze`)
		.then(response => {
			this.setState(response.data)
		})
		.catch(error=> {
		  console.log(error);
		})
	}
	
    render() {
		const clinicalData = this.state.clinicalData;
		let rows =clinicalData.map((item, index) => {
			 	return	(<tr>
						  <td>{++index}</td>
						  <td>{item.componentName}</td>
						  <td>{item.componentName}</td>
						</tr>)
						})
        return (
            <div>
				  <h1>Patient Details</h1>
				  First name : {this.state.firstName}<br/>
				  Last name : {this.state.lastName}<br/>
				  Age : {this.state.age}
				  <h2>clinical Analyze Data</h2>
					 <table>
					  <thead>
						<tr>
						  <th>S/N</th>
						  <th>Component Name</th>
						  <th>Component Name</th>
						</tr>
					  </thead>
					  <tbody>
							{rows}
					  </tbody>
			</table>
				 <Link to={'/'}>Go Back</Link>
            </div>
        );
    }
}

export default AnalyzeData;