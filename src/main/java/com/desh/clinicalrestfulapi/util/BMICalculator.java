package com.desh.clinicalrestfulapi.util;

import java.util.List;
import com.desh.clinicalrestfulapi.model.ClinicalData;

public class BMICalculator {
	public static void calculateBMI(List<ClinicalData> clinicalData, ClinicalData item) {
		if (item.getComponentName().equals("hw")) {
			String[] heightAndWeight = item.getComponentValue().split("/");

			if (heightAndWeight != null && heightAndWeight.length > 1) {
				float heightToMetres = Float.parseFloat(heightAndWeight[0]) * 0.4536F;
				float bmi = Float.parseFloat(heightAndWeight[1]) / (heightToMetres * heightToMetres);
				ClinicalData bmiData = new ClinicalData();
				bmiData.setComponentName("BMI");
				bmiData.setComponentValue(Float.toString(bmi));
				clinicalData.add(bmiData);
			}
		}
	}
}
