package com.desh.clinicalrestfulapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClinicalrestfulapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClinicalrestfulapiApplication.class, args);
	}

}
