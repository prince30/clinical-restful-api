package com.desh.clinicalrestfulapi.service;

import java.util.List;

import com.desh.clinicalrestfulapi.dto.ClinicalDataRequest;
import com.desh.clinicalrestfulapi.model.ClinicalData;

public interface ClinicalDataService {
	ClinicalData save(ClinicalDataRequest clinicalDataRequest);

	List<ClinicalData> getClinicalData(Long patientId, String componentName);
}
