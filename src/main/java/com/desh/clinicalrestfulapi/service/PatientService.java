package com.desh.clinicalrestfulapi.service;

import java.util.List;
import com.desh.clinicalrestfulapi.model.Patient;

public interface PatientService {

	Patient save(Patient patient);

	Patient getById(Long Id);

	List<Patient> getAll();
	
	Patient analyze(Long Id);
}
