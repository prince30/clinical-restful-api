package com.desh.clinicalrestfulapi.service;

import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.desh.clinicalrestfulapi.model.ClinicalData;
import com.desh.clinicalrestfulapi.model.Patient;
import com.desh.clinicalrestfulapi.repository.PatientRepository;
import com.desh.clinicalrestfulapi.util.BMICalculator;

@Service
public class PatientServiceImpl implements PatientService {

	@Autowired
	private PatientRepository patientRepository;

	Map<String, String> filters = new HashMap<>();

	@Override
	public Patient save(Patient patient) {
		return patientRepository.save(patient);
	}

	@Override
	public Patient getById(Long Id) {
		return patientRepository.findById(Id).get();
	}

	@Override
	public List<Patient> getAll() {
		return patientRepository.findAll();
	}

	@Override
	public Patient analyze(Long Id) {
		Patient patient = patientRepository.findById(Id).get();
		List<ClinicalData> clinicalData = patient.getClinicalData();
		List<ClinicalData> duplicateClinicalData = new ArrayList<>(clinicalData);

		for (ClinicalData item : duplicateClinicalData) {

			if (filters.containsKey(item.getComponentName())) {
				clinicalData.remove(item);
				continue;
			}
			else {

				filters.put(item.getComponentName(), null);
			}

			BMICalculator.calculateBMI(clinicalData, item);
		}
		filters.clear();
		return patient;
	}
}
