package com.desh.clinicalrestfulapi.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.desh.clinicalrestfulapi.dto.ClinicalDataRequest;
import com.desh.clinicalrestfulapi.model.ClinicalData;
import com.desh.clinicalrestfulapi.model.Patient;
import com.desh.clinicalrestfulapi.repository.ClinicalDataRepository;
import com.desh.clinicalrestfulapi.util.BMICalculator;

@Service
public class ClinicalDataServiceImpl implements ClinicalDataService {

	@Autowired
	private ClinicalDataRepository clinicalDataRepository;

	@Autowired
	private PatientService patientService;

	@Override
	public ClinicalData save(ClinicalDataRequest clinicalDataRequest) {
		Patient patient = patientService.getById(clinicalDataRequest.getPatientId());
		ClinicalData data = new ClinicalData();
		data.setComponentName(clinicalDataRequest.getComponentName());
		data.setComponentValue(clinicalDataRequest.getComponentValue());
		data.setPatient(patient);
		return clinicalDataRepository.save(data);
	}

	public List<ClinicalData> getClinicalData(Long patientId, String componentName) {

		if (componentName.equals("bmi")) {
			componentName = "hw";
		}

		List<ClinicalData> clinicalData = clinicalDataRepository
				.findByPatientIdAndComponentNameOrderByMeasureDateTime(patientId, componentName);
		List<ClinicalData> duplicateClinicalData = new ArrayList<>(clinicalData);

		for (ClinicalData item : duplicateClinicalData) {
			BMICalculator.calculateBMI(clinicalData, item);
		}

		return clinicalData;
	}
}
