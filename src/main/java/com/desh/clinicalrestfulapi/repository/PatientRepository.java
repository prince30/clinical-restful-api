package com.desh.clinicalrestfulapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.desh.clinicalrestfulapi.model.Patient;

public interface PatientRepository extends JpaRepository<Patient, Long> {

}
