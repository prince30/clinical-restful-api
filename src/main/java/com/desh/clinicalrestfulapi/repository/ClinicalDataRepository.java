package com.desh.clinicalrestfulapi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.desh.clinicalrestfulapi.model.ClinicalData;

public interface ClinicalDataRepository extends JpaRepository<ClinicalData, Long> {

	List<ClinicalData> findByPatientIdAndComponentNameOrderByMeasureDateTime(Long patientId, String componentName);

}
