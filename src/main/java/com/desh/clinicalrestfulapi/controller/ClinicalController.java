package com.desh.clinicalrestfulapi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.desh.clinicalrestfulapi.dto.ClinicalDataRequest;
import com.desh.clinicalrestfulapi.model.ClinicalData;
import com.desh.clinicalrestfulapi.service.ClinicalDataService;

@RestController
@RequestMapping("/api/v1/clinicals")
@CrossOrigin
public class ClinicalController {

	@Autowired
	private ClinicalDataService clinicalDataService;

	@PostMapping
	public ClinicalData store(@RequestBody ClinicalDataRequest clinicalDataRequest) {
		return clinicalDataService.save(clinicalDataRequest);
	}
	
	@GetMapping("/{patientId}/{componentName}")
	public List<ClinicalData> getClinicalData(@PathVariable("patientId") Long patientId,
			@PathVariable("componentName") String componentName) {
		return clinicalDataService.getClinicalData(patientId, componentName);
	}
}
