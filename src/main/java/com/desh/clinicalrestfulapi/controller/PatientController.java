package com.desh.clinicalrestfulapi.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.desh.clinicalrestfulapi.model.Patient;
import com.desh.clinicalrestfulapi.service.PatientService;

@RestController
@RequestMapping("/api/v1/patients")
@CrossOrigin
public class PatientController {

	@Autowired
	private PatientService patientService;

	@GetMapping
	public List<Patient> getAll() {
		return patientService.getAll();
	}

	@GetMapping("/{id}")
	public Patient show(@PathVariable("id") Long id) {
		return patientService.getById(id);
	}

	@PostMapping
	public Patient store(@RequestBody Patient patient) {
		return patientService.save(patient);
	}
	
	@GetMapping("/{id}/analyze")
	public Patient analyze(@PathVariable("id") Long id) {
		return patientService.analyze(id);
	}
}
